﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace Data
{
    public class BaseEntity: IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Gendre { get; set; }
        public DateTime DateOfBirth { get; set; }

        public string Image { get; set; }

    }
}
